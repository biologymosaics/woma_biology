



"""
	from WOMA_BIOLOGY.CONCEPTS.POLY import CREATE_POLY
	
	POLY =  CREATE_POLY (
		CONCEPTS = [
			{ 
				"STRING": 'python3 -m http.server 9000',
				"CWD": None
			},
			{
				"STRING": 'python3 -m http.server 9001',
				"CWD": None
			}
		]
	)
	
	EXIT = POLY["EXIT"]
	
	
	EXIT ()
	
"""

"""
	POLY = CREATE_POLY (
		CONCEPTS = [
			'python3 -m http.server 9000', 
			'python3 -m http.server 9001'
		],
		
		#
		#	IF WAIT IS TRUE, STOPS CONTINUATION OF THE SCRIPT
		#
		#	IF WAIT IS FALSE, THE CONCEPTS EXIT WHEN PARENT CONCEPT EXITS
		#
		WAIT = True
	)
"""

from ..CREATE import CREATE as CREATE_CONCEPT

from subprocess import Popen
import shlex
import atexit

def CREATE_POLY (
	CONCEPTS = [],
	WAIT = False
):
	PROCS = []

	for PROCESS in CONCEPTS:
		if (type (PROCESS) == str):
			print ("STARTING PROCESS", PROCESS)
		
			PROCS.append (
				Popen (
					shlex.split (PROCESS)
				)
			)
			
		elif (type (PROCESS) == dict):
			print ("STARTING PROCESS", PROCESS)
		
			PROCESS_STRING = PROCESS["STRING"]
		
			CWD = None
			ENV = None
		
			if ("CWD" in PROCESS):
				CWD = PROCESS["CWD"]
			
			if ("ENV" in PROCESS):
				ENV = PROCESS["ENV"]
		
			PROCS.append (
				Popen (
					shlex.split (PROCESS_STRING),
					
					cwd = CWD,
					env = ENV
				)
			)

	
	def EXIT ():
		print ("ATEXIT EXITING")

		for PROCESS in PROCS:
			print ("ENDING PROCESS ATEXIT", PROCESS);
			PROCESS.kill ()

		#exit ();

	atexit.register (EXIT)
	
			
	if (WAIT):
		for P in PROCS:
			#
			#	https://docs.python.org/3/library/subprocess.html#subprocess.Popen.wait
			#
			P.wait ()	
		
	return {
		"CONCEPTS": CONCEPTS,
		"EXIT": EXIT
	}
	
	
	
	