
"""
	from os.path import normpath, join, dirname
	
	from WOMA_BIOLOGY.SEAHORSE.SQLITE.ORGAN.CREATE 			import CREATE as CREATE_SQLITE_DB
	
	CREATE_SQLITE_DB (
		DB_LABEL = normpath (join (dirname (__file__), "")),
	)
"""


"""
https://docs.python.org/3/library/sqlite3.html#sqlite3.connect

sqlite3.connect (
	database, 
	timeout = 5.0, 
	detect_types = 0, 
	isolation_level = 'DEFERRED', 
	check_same_thread = True, 
	factory = sqlite3.Connection, 
	cached_statements = 128, 
	uri = False
)
"""
def CREATE (
	DB_LABEL = "SQLITE.DB"
):
	import sqlite3
	con = sqlite3.connect (DB_LABEL)
	
	print ("??")
	
	return con;