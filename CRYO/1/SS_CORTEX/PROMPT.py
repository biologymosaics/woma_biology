

"""
	from WOMA_BIOLOGY.SS_CORTEX.PROMPT import PROMPT
	TEXT = PROMPT ("???")
"""



def PROMPT (
	TEXT,
	SECRET = False
):
	from prompt_toolkit import prompt
	
	if (SECRET):
		return prompt (TEXT, is_password = True);
	
	return prompt (TEXT)