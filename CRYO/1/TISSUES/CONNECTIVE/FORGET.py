



"""
	* FORGET (
		ERASE
	)
"""

"""
	https://askubuntu.com/questions/122549/how-to-shred-a-folder
	https://askubuntu.com/questions/57572/how-to-delete-files-in-secure-manner
"""

"""
SHRED:
	APIRATIONS:
		SECURE DELETE ext4 folder ????
	
	PREFERABLE: ???
		
		SECURE DELETE:
		
			#
			#	l -> 2 writes
			#
			#
			#			
			<] srm -vfzrl FOLDER_1/*
			
			#
			#	ll -> 1 write
			#			
			<] srm -vfzrll FOLDER_1/*
		
		WIPE:
			<] wipe -q -Q 3 *
		
			<] wipe -r {folder}
			
			QUICKMODE, ONE WRITE:
				<] wipe -q -Q 1 -r {folder}
			
			QUICKMODE, THREE WRITES:
				<] wipe -q -Q 3 -r {folder}
	
	RANKED:
		WIPE:
			<] sudo apt install wipe
	
			<] wipe -rfi FOLDER_1/*
			
				# r -> recurse into folders (symlinks not followed)
				# f -> force, i.e. don't ask for confirmation
				# i -> informative, i.e. verbose
			
			
				# 34 passes..... yikes...
			
		SHRED:
		
			#
			#	MUCH MUCH MUCH FASTER THAN WIPE
			#
			[CONFIRMED] shred -uvzn 3 ________
			
			
		SECURE DELETE:
		
			<] sudo apt install secure-delete
		
			<] srm -vz FOLDER_1/*
			
				# v -> verbose
				# f -> fast
				
			<] srm -vfzr 
			
			
			<] srm -vfzrll FOLDER_1/*
"""

def FORGET ():
	SCRIPT = f""

	return;