

"""
	ASPIRATIONS:
		
		LIST = GET_LIST (
			PATH,
			
			REL_PATHS = True
		)
		
		
		LIST [{
			"PATH": "",
			"KIND": ""
		},{
			"PATH": "",
			"KIND": ""
		},{
			"PATH": "",
			"KIND": ""
		},{
			"PATH": "",
			"KIND": ""
		}]
		
		KINDS:
			E	EPITHELIAL
			C	CONNECTIVE
			N	NERVOUS
"""

from os.path import isfile, islink, isdir
from os.path import normpath, join

def GET_TISSUES_IN_CONNECTIVE (PATH):
	LIST = []

	import os
	TISSUES = os.listdir (PATH)
	
	for TISSUE in TISSUES:
		FULL_PATH = normpath (join (PATH, TISSUE))
		
		if (isfile (FULL_PATH)):
			LIST.append ({
				"PATH": FULL_PATH,
				"KIND": "E"
			})
			
	
		elif (isdir (FULL_PATH)):
			LIST.append ({
				"PATH": FULL_PATH,
				"KIND": "C"
			})
			
			ATTACHED_TISSUES = GET_TISSUES_IN_CONNECTIVE (
				FULL_PATH
			)
			for T in ATTACHED_TISSUES:
				LIST.append (T)
			
		elif (islink (FULL_PATH)):
			LIST.append ({
				"PATH": FULL_PATH,
				"KIND": "N"
			})
			
			
		else:
			raise Exception (f"Type of { FULL_PATH } could not be determined.")
		
	
	return LIST


def GET_LIST (
	PATH,
	REL_PATHS = False
):
	LIST = GET_TISSUES_IN_CONNECTIVE (PATH)
	
	if (REL_PATHS):
		for ENTRY in LIST:
			_PATH = ENTRY ["PATH"]
			ENTRY ["PATH"] = ENTRY ["PATH"] [
				(len (PATH) + 1): len (_PATH)
			]
		
	return LIST