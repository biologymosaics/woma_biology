

"""
	from WOMA_BIOLOGY.UNITS.GRAMS.TO_OUNCES import GRAMS_TO_OUNCES
"""

"""
	
	INTERNATIONAL AVOIRDUPOIS OUNCE:
		https://en.wikipedia.org/wiki/International_yard_and_pound
		
		1 OUNCE = 28.349523125 GRAMS
"""

"""
		1 OUNCE = Fraction (28.349523125) GRAMS
"""

def GRAMS_TO_OUNCES (GRAMS):
	return (28.349523125 / GRAMS)
	
	
	
