
"""
	from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.CONSTANTS import DAYS_BEFORE_MONTH
	_DAYS_BEFORE_MONTH = DAYS_BEFORE_MONTH ()[12]
	
	from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.CONSTANTS import DAYS_BEFORE_MONTH_LEAP
	_DAYS_BEFORE_MONTH_LEAP = DAYS_BEFORE_MONTH_LEAP ()[12]
"""

"""
	JAN -> 31
	FEB -> 28 or 29
	MAR -> 31
	APR -> 30
	
	MAY -> 31
	JUN -> 30
	JUL -> 31
	AUG -> 31
	
	SEP -> 30
	OCT -> 31
	NOV -> 30
	DEC -> 31
"""

#
#	IF LEAP YEAR & AFTER:
#
#		DAYS_BEFORE_MONTH [ 1 ] = 0		# 0	 DAYS HAVE OCCURRED BEFORE JANUARY
#		DAYS_BEFORE_MONTH [ 2 ] = 31	# 31 DAYS HAVE OCCURRED BEFORE FEBRUARY
#
def DAYS_BEFORE_MONTH ():
	return [
		None,
		0, 		# JAN
		31, 	# FEB
		59,		# MARCH 		(31 + 28)
		90,		# APRIL			(31 + 28 + 31)
		120,	# MAY			(31 + 28 + 31 + 30)
		151,	# JUNE			(31 + 28 + 31 + 30 + 31)
		181,	# JULY			(31 + 28 + 31 + 30 + 31 + 30)
		212,	# AUGUST		(31 + 28 + 31 + 30 + 31 + 30 + 31)
		243,	# SEPTEMBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31)
		273,	# OCTOBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31+ 30)
		304,	# NOVEMBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31+ 30 + 31)
		334,	# DECEMBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31+ 30 + 31 + 30)
		
		# 334 + 31 = 365
	]


#
#	IF LEAP YEAR & AFTER:
#
#		DAYS_BEFORE_MONTH [ 1 ] = 0		# 0	 DAYS HAVE OCCURRED BEFORE JANUARY
#		DAYS_BEFORE_MONTH [ 2 ] = 31	# 31 DAYS HAVE OCCURRED BEFORE FEBRUARY
#
def DAYS_BEFORE_MONTH_LEAP ():
	return [
		None,
		0, 		# JAN
		31, 	# FEB
		60,		# MARCH 		(31 + 29)
		91,		# APRIL			(31 + 28 + 31)
		121,	# MAY			(31 + 28 + 31 + 30)
		152,	# JUNE			(31 + 28 + 31 + 30 + 31)
		182,	# JULY			(31 + 28 + 31 + 30 + 31 + 30)
		213,	# AUGUST		(31 + 28 + 31 + 30 + 31 + 30 + 31)
		244,	# SEPTEMBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31)
		274,	# OCTOBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31+ 30)
		305,	# NOVEMBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31+ 30 + 31)
		335,	# DECEMBER		(31 + 28 + 31 + 30 + 31 + 30 + 31 + 31+ 30 + 31 + 30)
		
		# 335 + 31 = 366
	]
	
