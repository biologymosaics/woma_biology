

"""
	from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.DAYS_IN_MONTH import DAYS_IN_MONTH
	_DAYS_IN_MONTH = DAYS_IN_MONTH (12, LEAP_YEAR = True)
"""


"""
	JAN -> 31
	FEB -> 28 or 29
	MAR -> 31
	APR -> 30
	
	MAY -> 31
	JUN -> 30
	JUL -> 31
	AUG -> 31
	
	SEP -> 30
	OCT -> 31
	NOV -> 30
	DEC -> 31
"""
def DAYS_IN_MONTH (M, LEAP_YEAR):
	if (type (M) != int):
		raise Exception (f"MONTH PROVIDED '{ M }' IS NOT BETWEEN 1 AND 12")

	if (M == 1): return 31;
	
	elif (M == 2): 
		if (LEAP_YEAR): return 29
		else: return 28
		
	elif (M == 3): 		return 31
	elif (M == 4): 		return 30
	
	elif (M == 5): 		return 31
	elif (M == 6): 		return 30
	elif (M == 7): 		return 31
	elif (M == 8): 		return 31
	
	elif (M == 9): 		return 30
	elif (M == 10): 	return 31
	elif (M == 11): 	return 30
	elif (M == 12): 	return 31

	return;