

"""
	from WOMA_BIOLOGY.RHYTM.EARTH_UTC.DAY_OF_YEAR import DAY_OF_YEAR
"""

from .IS_LEAP_YEAR import IS_LEAP_YEAR
from .DAYS_IN_MONTH import DAYS_IN_MONTH

from .CONSTANTS import DAYS_BEFORE_MONTH
from .CONSTANTS import DAYS_BEFORE_MONTH_LEAP



def DAY_OF_YEAR (
	YEAR,
	MONTH,
	DAY_OF_MONTH
):
	if (type (YEAR) != int):
		raise Exception (f"YEAR NEEDS TO BE TYPE INT, GOT '{ YEAR }'")
	
	assert (YEAR != 0)
	assert (YEAR != 0)
	
	if (type (MONTH) != int):
		raise Exception (f"MONTH NEEDS TO BE TYPE INT, GOT '{ MONTH }'")
	
	assert (MONTH >= 1)
	assert (MONTH <= 12)
	
	
	if (type (DAY_OF_MONTH) != int):
		raise Exception (f"DAY_OF_MONTH NEEDS TO BE TYPE INT, GOT '{ DAY_OF_MONTH }'")


	LEAP_YEAR = IS_LEAP_YEAR (YEAR);
	
	DAY_OF_YEAR = 0
	if (LEAP_YEAR):
		DAY_OF_YEAR += DAYS_BEFORE_MONTH_LEAP ()[ MONTH ]
	else:
		DAY_OF_YEAR += DAYS_BEFORE_MONTH ()[ MONTH ]
	
	print ("???", DAY_OF_YEAR )
	
	DAY_OF_YEAR += DAY_OF_MONTH
	
	return DAY_OF_YEAR