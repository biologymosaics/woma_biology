

"""
	from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.IS_LEAP_YEAR import IS_LEAP_YEAR
	
	LEAP_YEAR = IS_LEAP_YEAR (3000)
"""

"""
	NOTES:
		MULTIPLES OF 400
		
		MULTIPLES OF 100
		
		
	-100	X
	0		LEAP YEAR
	100		X
	200		X
	300		X
	400		LEAP YEAR
	...
	800		
	...
	1200		
	...
	1600	
	2000	
	
	
	
"""
def IS_LEAP_YEAR (YEAR):
	if (type (YEAR) != int):
		raise Exception (f"IS_LEAP_YEAR(YEAR) NEEDS 'YEAR' TO BE 'int', GOT: '{ YEAR }'")

	Y = YEAR
	
	#
	#	0 IS NOT A VALID UTC YEAR
	#
	#		GOES FROM: INFINITY BC TO 1 BC, THEN 1 AD TO INFINITY
	#
	if (Y == 0):
		raise Exception ("0 IS NOT A VALID UTC YEAR")
	
	if (Y % 400 == 0):
		return True;
		
	if (Y % 100 == 0):
		return False
		
	if (Y % 4 == 0):
		return True
		
	return False

