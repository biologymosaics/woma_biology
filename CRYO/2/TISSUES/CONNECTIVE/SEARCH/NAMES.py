

'''
	LINUX:
		??? FINDS ONE...
			>> find . -type f,d -name "TISSUES"

			   -type c
					  File is of type c:

					  b      block (buffered) special

					  c      character (unbuffered) special

					  d      directory

					  p      named pipe (FIFO)

					  f      regular file

					  l      symbolic link; this is never true if the -L option or 
							 the -follow option is in effect,  unless  the  symbolic
							 link is broken.  If you want to search for symbolic links when -L is in effect, use -xtype.


					  s      socket

					  D      door (Solaris)

					  To  search for more than one type at once, you can supply the combined list of type letters separated by a comma `,'
					  (GNU extension).


	CUSTOM:
		ITERATE THROUGH TISSUES...
'''