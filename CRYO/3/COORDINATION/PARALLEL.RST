

'''


def ACTION (PARAMS):
	print ("ACTION", PARAMS)

	Z = 1
	while Z <= 3:
		print (PARAMS)

		import time
		time.sleep (1)
		
		Z += 1

	return PARAMS;

from multiprocessing import Pool
POOL = Pool ()

POOL_1 = POOL.apply_async (ACTION, [ 1 ])
POOL_2 = POOL.apply_async (ACTION, [ 2 ])

FINISH_1 = POOL_1.get (timeout = 10)
FINISH_2 = POOL_2.get (timeout = 10)

print ("FINISH_1:", FINISH_1)
print ("FINISH_2:", FINISH_2)

'''