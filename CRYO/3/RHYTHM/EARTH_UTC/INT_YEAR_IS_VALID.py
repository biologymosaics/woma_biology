
"""
	from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.INT_YEAR_IS_VALID import INT_YEAR_IS_VALID 
	YEAR_IS_VALID = INT_YEAR_IS_VALID (2000)
"""


def INT_YEAR_IS_VALID (YEAR):
	try:
		assert (type (YEAR) == int)
		assert (YEAR >= 1 or YEAR <= -1)

		return True;
	except Exception:
		pass
		
	return False
