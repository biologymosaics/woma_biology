

"""
	from WOMA_BIOLOGY.SS_CORTEX.SINGLE_SELECT_2 import SINGLE_SELECT_2
	
	SELECTION = SINGLE_SELECT_2 (
		OPTIONS = [{
			"LABEL": "ONE"
			"KEY": "1"
		},{
			"LABEL": "TWO",
			"KEY": "2"
		}]
	)
"""


from simple_term_menu import TerminalMenu


def SINGLE_SELECT_2 (
	OPTIONS
):
	def PARSE_OPTION (OPTION):
		return OPTION["LABEL"]

	PARSED = list (map (PARSE_OPTION, OPTIONS))

	#print ("PARSED_OPTIONS", PARSED)

	terminal_menu = TerminalMenu (PARSED)
	INDEX = terminal_menu.show ()

	return {
		"OPTION": OPTIONS [ INDEX ],
		"INDEX" : INDEX
	}

	

