

"""
	WIPE:
	<] wipe -r {folder}
	
	QUICKMODE, ONE WRITE:
		<] wipe -q -Q 1 -r {folder}
	
	QUICKMODE, THREE WRITES:
		<] wipe -q -Q 3 -r {folder}
"""

"""
	https://github.com/berke/wipe
"""

"""
	SECURE DELETE:
		srm
"""