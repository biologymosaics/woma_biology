#!/usr/bin/python3

#


FREEZE_NUMBER = "3";


#


import os
from os.path import dirname
from os.path import join
from os.path import normpath

import sys

PATHS = [ ]

from pathlib import Path
HERE = Path (__file__).parent.absolute ()

STEMS = normpath (join (HERE, "STEMS"))
FREEZE = normpath (join (HERE, "CRYO/" + FREEZE_NUMBER))
FREEZE_NUMBER_PATH = normpath (join (HERE, "CRYO/" + FREEZE_NUMBER + "/FREEZE.NUMBER"))


#import shutil
#shutil.copyfile (STEMS, FREEZE)

print (STEMS, FREEZE)

#import os,stat
#os.chmod (FREEZE, stat.S_IWRITE) 
import subprocess
subprocess.run(['cp', '-Rv', STEMS, FREEZE ])

#exit ()

WFP = open (FREEZE_NUMBER_PATH, "w")
WFP.write (FREEZE_NUMBER)
WFP.close ()

#subprocess.run ([ 'sudo', 'chmod', '-R', '0555', FREEZE ])
subprocess.run ([ 'chmod', '-R', '0555', FREEZE ])

