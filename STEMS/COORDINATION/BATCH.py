




"""
	from WOMA_BIOLOGY.COORDINATION.BATCH import BATCH
	


	import asyncio
	from datetime import datetime

	async def PROCEDURE__ (
		INDEX = False,
		#
		#
		PARAMS = ""
	):
		WAIT_FOR = 1
	
		if (type (PARAMS) == list):
			WAIT_FOR = PARAMS [1];
		
		NOW = datetime.now()
		print(f'[{ NOW }] PROCEDURE {INDEX}: STARTED')
		#print ('	PARAMS:', PARAMS)
		#print ();
		
		# I/O, context will switch to main function
		await asyncio.sleep (WAIT_FOR)  
		
		print(f'[{ NOW }] PROCEDURE {INDEX}: CONCLUDED')


	RESULTS = [];

	asyncio.run(BATCH (
		FUNCTION = PROCEDURE__,
		PARAMS = [
			[ "1", 6 ],
			[ "2", 6 ],
			"3",
			"4",
			"5",
			"6",
			"7"
		],
		MAX = 3
	))
"""

import asyncio

async def BATCH (
	FUNCTION = False,
	MAX = 10,
	PARAMS = []
):
	RESULTS = [];
	LOOP = asyncio.get_event_loop ()

	
	#
	# SEMAPHORES LIMIT THE NUMBER OF
	# ASYNC ACTIONS
	#
	SEM = asyncio.Semaphore (MAX)

	async def START (I, ACTIVITY):
		await SEM.acquire ()
		
		try:
			INDEX = I[0]
					
			await ACTIVITY (
				INDEX = I[0],
				PARAMS = PARAMS[INDEX]
			)
						
		finally:
			SEM.release()
			
		return;
	
	
	# TASK CREATION STARTS THE COROUTINES
	#
	#	https://docs.python.org/3/library/asyncio-future.html?highlight=ensure_future
	#
	TASKS = [
		asyncio.ensure_future (
			START (I, FUNCTION)
		)
		for I in enumerate (PARAMS)
	]
	

	#
	# WAIT FOR ALL OF THE 
	# FUNCTIONS TO COMPLETE
	#
	#	https://docs.python.org/3/library/asyncio-task.html?highlight=gather#asyncio.gather
	#
	await asyncio.gather (* TASKS)

	return