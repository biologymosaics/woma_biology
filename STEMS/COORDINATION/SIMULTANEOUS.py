

'''
	https://stackoverflow.com/questions/40391898/send-simultaneous-requests-python-all-at-once

	import requests
	from concurrent.futures import ThreadPoolExecutor

	def get_url(url):
		return requests.get(url)
		
	with ThreadPoolExecutor(max_workers=50) as pool:
		print(list(pool.map(get_url,list_of_urls)))
'''