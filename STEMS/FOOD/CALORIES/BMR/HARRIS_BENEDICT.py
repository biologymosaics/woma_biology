
'''
	from WOMA_BIOLOGY.FOOD.CALORIES.BMR.HARRIS_BENEDICT import HARRIS_BENEDICT
	
	HARRIS_BENEDICT (
		RECEIVES = {
			
			#
			#	[ "GUY", "GAL" ]
			#
			"GENDER": "GUY",
			
			#
			#	KILOGRAMS
			#
			"WEIGHT": "91",
			
			#
			#	CM (CETERMETERS)
			#
			"HEIGHT": "182.88",
			
			#
			#	IN ORBITS
			#
			"AGE": "",
			
			#
			#	[ "ROZA AND SHIZGAL", "MIFFLIN AND ST JEOR" ] 
			#
			"REVISION": "MIFFLIN AND ST JEOR"
		},
		
		SENDS = [
			"BMR"
		]
	)
'''

'''
	6 FEET = 1.8288 METERS = 182.88 CM
	
	200 POUNDS = 200 (0.45359237) KG = 90.718474 KG
'''

from fractions import Fraction as F

def HARRIS_BENEDICT (
	RECEIVES = {
		
		#
		#	[ "GUY", "GAL" ]
		#
		"GENDER": "",
		
		"WEIGHT": "",
		"HEIGHT": "",
		"AGE": "",
		
		#
		#	[ "ROZA AND SHIZGAL", "MIFFLIN AND ST JEOR" ] 
		#
		"REVISION": "MIFFLIN AND ST JEOR",
		
		"PREGNANT": {
			#
			#	0 TO 1 -> 1 = 9 MONTHS
			#
			"PROGRESS": 0,
			
			#
			#	0 TO ...
			#
			"PROGYNIES": 0
		}
	},
	
	SENDS = {
		
	}
):
	R = RECEIVES;
	
	REVISION 	= R["REVISION"]

	GENDER 		= R["GENDER"]

	WEIGHT 		= F (R["WEIGHT"])
	HEIGHT 		= F (R["HEIGHT"])
	AGE 		= F (R["AGE"])
	
	if (REVISION == "MIFFLIN AND ST JEOR"):
		if (GENDER == "GUY"):
			# BMR = (10 × weight in kg) + (6.25 × height in cm) – (5 × age in years) + 5 
			
			BMR = (
				(10 * WEIGHT) +
				(F (6.25) * HEIGHT) +
				(5 * AGE) + 
				5
			)
			
			print (BMR)
			
			return {
				"BMR": float (BMR)
			}
		
		if (GENDER == "GAL"):
			# BMR = (10 × weight in kg) + (6.25 × height in cm) – (5 × age in years) – 161 
		
			BMR = (
				(10 * WEIGHT) +
				(F (6.25) * HEIGHT) +
				(5 * AGE) -
				161
			)
			
			return {
				"BMR": float (BMR)
			}

	
	
	
	return;
	
	
	
