

"""
	from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.INT_DAY_OF_MONTH_IS_VALID import INT_DAY_OF_MONTH_IS_VALID 
	MONTH_IS_VALID = INT_DAY_OF_MONTH_IS_VALID (32, 1, False)
"""

from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.DAYS_IN_MONTH import DAYS_IN_MONTH
from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.IS_LEAP_YEAR import IS_LEAP_YEAR
	
	
def INT_DAY_OF_MONTH_IS_VALID (YEAR, MONTH, DAY_OF_MONTH):
	try:
		assert (type (DAY_OF_MONTH) == int)
		assert (DAY_OF_MONTH >= 1)
		
		assert (type (MONTH) == int)
		assert (MONTH >= 1)
		assert (MONTH <= 12)
		
		assert (type (YEAR) == int)
		assert (YEAR >= 1 or YEAR <= -1)
		
		LEAP_YEAR = IS_LEAP_YEAR (YEAR)
		assert (type (LEAP_YEAR) == bool)

		print ("LEAP YEAR", LEAP_YEAR, YEAR, MONTH, DAY_OF_MONTH)

		if (LEAP_YEAR == True):
			_DAYS_IN_MONTH = DAYS_IN_MONTH (MONTH, LEAP_YEAR = LEAP_YEAR)
			assert (DAY_OF_MONTH <= _DAYS_IN_MONTH)
			return True;
		
		if (LEAP_YEAR == False):
			_DAYS_IN_MONTH = DAYS_IN_MONTH (MONTH, LEAP_YEAR = LEAP_YEAR)
			assert (DAY_OF_MONTH <= _DAYS_IN_MONTH)
			return True;
			
	except Exception:
		pass
		
	return False