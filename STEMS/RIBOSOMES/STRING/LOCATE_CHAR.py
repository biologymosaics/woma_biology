
"""
	-1 -> NOT FOUND
"""

"""
	from WOMA_BIOLOGY.RIBOSOMES.STRING.LOCATE_CHAR import LOCATE_CHAR_INDEX
	
	INDEX = LOCATE_CHAR_INDEX (CHAR, STRING);
"""

"""
	from WOMA_BIOLOGY.RIBOSOMES.STRING.LOCATE_CHAR import LOCATE_CHAR_SPOT
	
	SPOT = LOCATE_CHAR_SPOT (CHAR, STRING);
"""

def LOCATE_CHAR_INDEX (
	CHAR,
	STRING,
	
	EVERY = False
):
	if (not EVERY):
		INDEX = 0
		for _CHAR_ in STRING:
			if (_CHAR_ == CHAR):
				return INDEX
			
			INDEX += 1
		
		return -1
		
	else:
		INDEXES = []
	
		INDEX = 0
		for _CHAR_ in STRING:
			if (_CHAR_ == CHAR):
				INDEXES.append (INDEX)
			
			INDEX += 1
		
		return INDEXES
	
	
def LOCATE_CHAR_SPOT (
	CHAR,
	STRING
):
	INDEX = LOCATE_CHAR_INDEX (CHAR, STRING)
	
	if (INDEX == -1):
		return INDEX
		
	return INDEX + 1;
		
	