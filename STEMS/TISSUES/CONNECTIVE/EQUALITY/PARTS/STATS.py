


def IS_DIRECTORY (FULL_PATH):
	import stat, os
	return stat.S_ISDIR (os.stat (FULL_PATH).st_mode)

def IS_REG_FILE (FULL_PATH):
	import stat, os
	return stat.S_ISREG (os.stat (FULL_PATH).st_mode)

def IS_SYMLINK (FULL_PATH):
	import stat, os
	return stat.S_ISLNK (os.stat (FULL_PATH).st_mode)