




"""
	from WOMA_BIOLOGY.TISSUES.CONNECTIVE.EQUALIZE.V2 import EQUALIZE
"""

"""
	POSSIBILITIES:
		https://pypi.org/project/rsyncy/
"""

"""
	* CLONE ()
"""

RSYNC_LOCATION = "rsync"

def EQUALIZE (
	FROM = "",
	TO = ""
):
	SCRIPT = f'{ RSYNC_LOCATION } --progress --delete -av "{ FROM }/" "{ TO }"';
	
	from WOMA_BIOLOGY.CONCEPTS.CREATE import CREATE as CREATE_CONCEPT
	CREATE_CONCEPT (
		SCRIPT
	)

	return;
	
CLONE = EQUALIZE