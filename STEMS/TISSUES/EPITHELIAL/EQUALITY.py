


"""
	WISHES:
		from WOMA_BIOLOGY.TISSUES.EPITHELIAL.EQUALITY import EQUALITY as EPITHELIAL_EQUALITY
		
		RESULTS = EQUALITY (
			normpath (join (dirname (__file__), "EXAMPLES/2_UNEQUAL/1")),
			normpath (join (dirname (__file__), "EXAMPLES/2_UNEQUAL/2"))
		);
"""

"""
	https://docs.python.org/3/library/filecmp.html
"""

import filecmp
def EQUALITY (E1, E2):
	return filecmp.cmp (E1, E2, shallow = False)