

'''
	USES CWD
	
	[TCSH] woma tissues connective eq --von "" --to ""
'''
def EQ (CONNECTIVE):
	import click
	@CONNECTIVE.command ("eq", help = '')
	@click.option ('--von', default = '')
	@click.option ('--to', required = True)
	def START (von, to):
		from .CONNECTIVE.EQUALIZE.V2 import EQUALIZE
		
		import os
		CWD = os.getcwd ()
		
		from os.path import normpath, dirname, join
		VON = normpath (join (CWD, von))
		TO = normpath (join (CWD, to))
		
		EQUALIZE (VON, TO)
	
		print ("???")

	return;


def TISSUES ():
	import click
	@click.group ()
	def TISSUES ():
		pass
		
	@TISSUES.group ()
	def CONNECTIVE ():
		pass

	EQ (CONNECTIVE)

	return TISSUES