



'''
	from WOMA_BIOLOGY.HTTPS.FLASK.EXAMPLES.EXAMPLE_1 import PORTS
'''

from os.path 							import exists, dirname, normpath, join

from flask 								import Flask, send_file, request, make_response
from flask 								import send_from_directory
from flask 								import Response
from flask 								import render_template

from flask_socketio 					import SocketIO, emit

from flask_limiter 						import Limiter
from flask_limiter.util 				import get_remote_address

# https://pypi.org/project/validator-collection/
from jsonschema import validate

'''
	https://flask-socketio.readthedocs.io/en/latest/getting_started.html
'''
def PORTS (
	TRAINING = True,
	PORT = ""
):
	if (type (PORT) != int or PORT < 1):
		raise Exception (f'INVALID PORT: "{ PORT }"')

	print ("PORT:", PORT)

	app = Flask (__name__)
	app.config ['SECRET_KEY'] = 'secret!'
	socketio = SocketIO (app)
	#SOCKETS (socketio, emit)


	from .ROUTES import HOME
	HOME (app)


	DEBUG = TRAINING;
	USE_RELOADER = TRAINING;
	socketio.run (
		app,
		port			= PORT,
		#use_reloader 	= True

		#debug	= DEBUG,
		
		#host	= "0.0.0.0",
		
	)
	

	return;