
'''
	./MONITORS start --path "CHECKS/FOOD/CALORIES/BMR"
'''

from WOMA_BIOLOGY.FOOD.CALORIES.BMR.HARRIS_BENEDICT import HARRIS_BENEDICT

def test_1 ():
	CALCULATION = HARRIS_BENEDICT (
		RECEIVES = {
			"GENDER": "GUY",
			"WEIGHT": "91",
			"HEIGHT": "182.88",
			"AGE": "32",
			"REVISION": "MIFFLIN AND ST JEOR"
		},
		
		SENDS = [
			"BMR"
		]
	)

	assert (type (CALCULATION["BMR"]) == float)
	assert (CALCULATION["BMR"] == 2218)
	
	
def test_2 ():
	CALCULATION = HARRIS_BENEDICT (
		RECEIVES = {
			"GENDER": "GUY",
			"WEIGHT": "92.414",
			"HEIGHT": "182.88",
			"AGE": "32",
			"REVISION": "MIFFLIN AND ST JEOR"
		},
		
		SENDS = [
			"BMR"
		]
	)
	
	assert (type (CALCULATION["BMR"]) == float)
	assert (CALCULATION["BMR"] == 2232.14)
	
	