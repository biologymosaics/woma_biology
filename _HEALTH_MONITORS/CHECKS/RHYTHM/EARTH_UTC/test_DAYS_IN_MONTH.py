


"""
	./CHECKS start --path "RHYTHM/EARTH_UTC/test_DAYS_IN_MONTH.py"
"""

from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.DAYS_IN_MONTH 	import DAYS_IN_MONTH



def test_DAYS_IN_MONTH ():
	assert DAYS_IN_MONTH (1, False) == 31
	
	assert DAYS_IN_MONTH (2, False) == 28
	assert DAYS_IN_MONTH (2, True) == 29
	

