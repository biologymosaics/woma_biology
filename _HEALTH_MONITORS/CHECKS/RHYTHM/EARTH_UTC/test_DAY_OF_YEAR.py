
"""
	./CHECKS start --path "RHYTHM/EARTH_UTC/test_DAY_OF_YEAR.py"
"""

from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.DAY_OF_YEAR import DAY_OF_YEAR


def ASSERT_EXCEPTION (Y, M, D):
	EXCEPTION_OCCURRED = False;

	try:
		DAY_OF_YEAR (Y, M, D)
	except Exception:
		EXCEPTION_OCCURRED = True;
		print (Exception)
	
	assert (EXCEPTION_OCCURRED)
	
	

def test_DAY_OF_YEAR ():
	assert DAY_OF_YEAR (1, 1, 1) 	== 1
	assert DAY_OF_YEAR (1, 1, 31) 	== 31
	assert DAY_OF_YEAR (1, 2, 1) 	== 32
	
	assert DAY_OF_YEAR (4001, 3, 1) == 60
	assert DAY_OF_YEAR (4000, 3, 1) == 61
	

def test_DAY_OF_YEAR_EXCEPTIONS ():
	ASSERT_EXCEPTION (0, 0, 0)