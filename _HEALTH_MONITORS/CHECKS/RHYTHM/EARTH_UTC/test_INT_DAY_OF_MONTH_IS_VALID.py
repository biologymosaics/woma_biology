

"""
	./CHECKS start --path "RHYTHM/EARTH_UTC/test_INT_DAY_OF_MONTH_IS_VALID.py"
"""

from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.INT_DAY_OF_MONTH_IS_VALID import INT_DAY_OF_MONTH_IS_VALID 

def ASSERT_EXCEPTION (Y, M, D):
	EXCEPTION_OCCURRED = False;

	try:
		INT_DAY_OF_MONTH_IS_VALID (D, M, L)
	except Exception:
		EXCEPTION_OCCURRED = True;
		print (Exception)
	
	assert (EXCEPTION_OCCURRED)


def test_True ():
	assert (INT_DAY_OF_MONTH_IS_VALID (3200, 2, 29) == True)
	assert (INT_DAY_OF_MONTH_IS_VALID (3001, 2, 28) == True)
	assert (INT_DAY_OF_MONTH_IS_VALID (3000, 2, 28) == True)




def test_False ():
	assert (INT_DAY_OF_MONTH_IS_VALID (3000, 2, False) == False)
	