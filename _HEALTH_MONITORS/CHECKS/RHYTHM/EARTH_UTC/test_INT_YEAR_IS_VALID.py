

"""
	./CHECKS start --path "RHYTHM/EARTH_UTC/test_INT_YEAR_IS_VALID.py"
"""

from WOMA_BIOLOGY.RHYTHM.EARTH_UTC.INT_YEAR_IS_VALID import INT_YEAR_IS_VALID 


def test_False ():
	assert (INT_YEAR_IS_VALID (0) == False)
	assert (INT_YEAR_IS_VALID ("0") == False)
	assert (INT_YEAR_IS_VALID (0) == False)
	assert (INT_YEAR_IS_VALID (0) == False)


def test_True ():
	assert (INT_YEAR_IS_VALID (2000) == True)
	assert (INT_YEAR_IS_VALID (900000000000000000000000000) == True)
