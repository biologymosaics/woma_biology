
"""


"""

from WOMA_BIOLOGY.RIBOSOMES.STRING.LOCATE_CHAR import LOCATE_CHAR_INDEX
from WOMA_BIOLOGY.RIBOSOMES.STRING.LOCATE_CHAR import LOCATE_CHAR_SPOT


#----------------------------------------------------------------------------

def test_MULTIPLE_1 ():
	INDEXES = LOCATE_CHAR_INDEX ("L", "MALSGPAUALS", EVERY = True);

	assert (len (INDEXES) == 2);
	
	assert (INDEXES[0] == 2)
	assert (INDEXES[1] == 9)

	return;


def test_MULTIPLE_2 ():
	INDEXES = LOCATE_CHAR_INDEX ("H", "MALSGPAUALS", EVERY = True);

	assert (len (INDEXES) == 0);

	return;



#----------------------------------------------------------------------------

def test_1 ():
	INDEX = LOCATE_CHAR_INDEX ("L", "MALSGPAUA");
	assert (INDEX == 2)


	SPOT = LOCATE_CHAR_SPOT ("L", "MALSGPAUA");
	assert (SPOT == 3)

	return;
	
	
def test_2 ():
	INDEX = LOCATE_CHAR_INDEX ("&", "MALSGPAUA");
	assert (INDEX == -1)


	SPOT = LOCATE_CHAR_SPOT ("&", "MALSGPAUA");
	assert (SPOT == -1)

	return;