
"""


"""

from WOMA_BIOLOGY.RIBOSOMES.STRING.LOCATE_STRING import LOCATE_STRING_INDEXES

#----------------------------------------------------------------------------

def test_MULTIPLE_1 ():
	LOCATIONS = LOCATE_STRING_INDEXES (
		"1", 
		"12121212",
		
		EVERY = True
	);
	
	assert (len (LOCATIONS) == 4)

	print (LOCATIONS)
	
	assert (LOCATIONS[0][0] == 0)
	assert (LOCATIONS[0][1] == 0)
	
	assert (LOCATIONS[1][0] == 2)
	assert (LOCATIONS[1][1] == 2)
	
	assert (LOCATIONS[2][0] == 4)
	assert (LOCATIONS[2][1] == 4)
	
	assert (LOCATIONS[3][0] == 6)
	assert (LOCATIONS[3][1] == 6)


def test_MULTIPLE_2 ():
	LOCATIONS = LOCATE_STRING_INDEXES (
		"12", 
		"1212",
		
		EVERY = True
	);
	
	print (LOCATIONS)
	
	assert (len (LOCATIONS) == 2)
	
	assert (LOCATIONS[0][0] == 0)
	assert (LOCATIONS[0][1] == 1)
	
	assert (LOCATIONS[1][0] == 2)
	assert (LOCATIONS[1][1] == 3)


def test_MULTIPLE_3 ():
	LOCATIONS = LOCATE_STRING_INDEXES (
		"123", 
		"123123123",
		
		EVERY = True
	);
	
	print (LOCATIONS)
	
	assert (len (LOCATIONS) == 3)
	
	assert (LOCATIONS[0][0] == 0)
	assert (LOCATIONS[0][1] == 2)
	
	assert (LOCATIONS[1][0] == 3)
	assert (LOCATIONS[1][1] == 5)
	
	assert (LOCATIONS[2][0] == 6)
	assert (LOCATIONS[2][1] == 8)


def test_MULTIPLE_4 ():
	LOCATIONS = LOCATE_STRING_INDEXES (
		"121", 
		"1212121212121",
		
		EVERY = True
	);
	
	print (LOCATIONS)
	
	assert (len (LOCATIONS) == 6)
	
	assert (str (LOCATIONS) == "[[0, 2], [2, 4], [4, 6], [6, 8], [8, 10], [10, 12]]")



def test_MULTIPLE_5 ():
	LOCATIONS = LOCATE_STRING_INDEXES (
		"ARGININE", 
		"AUG METHIONINE ARGININE LEUCINE SERINE GLYCINE PROLINE ARGININE UAG",
		
		EVERY = True
	);
	
	assert (len (LOCATIONS) == 2)
	
	assert (LOCATIONS[0][0] == 15)
	assert (LOCATIONS[0][1] == 22)
	
	assert (LOCATIONS[1][0] == 55)
	assert (LOCATIONS[1][1] == 62)

#----------------------------------------------------------------------------

"""
def test_0 ():
	INDEXES = LOCATE_STRING_INDEXES (
		"ASPARAGINE", 
		"AUG METHIONINE LEUCINE SERINE GLYCINE PROLINE ARGININE UAG"
	);
	
	assert (INDEXES[0] == -1)
	assert (INDEXES[1] == -1)

def test_1 ():
	INDEXES = LOCATE_STRING_INDEXES (
		"UAG", 
		"AUG METHIONINE ASPARAGINE LEUCINE SERINE GLYCINE PROLINE ARGININE UAG ASPARAGINE"
	);
	
	assert (INDEXES[0] == 66)
	assert (INDEXES[1] == 68)
	
def test_2 ():
	INDEXES = LOCATE_STRING_INDEXES (
		"L", 
		"AUG METHIONINE ASPARAGINE LEUCINE SERINE GLYCINE PROLINE ARGININE UAG ASPARAGINE"
	);
	
	assert (INDEXES[0] == 26)
	assert (INDEXES[1] == 26)
"""
