
"""


"""

from WOMA_BIOLOGY.SEAHORSE.SQLITE.ORGAN.CREATE import CREATE as CREATE_SQLITE_DB
from os.path import normpath, join, dirname
import os

from WOMA_BIOLOGY.TISSUES.CONNECTIVE.ABANDON 		import ABANDON as ABANDON_CONNECTIVE

#----------------------------------------------------------------------------

def test_MULTIPLE_1 ():
	ORGAN_PATH = normpath (join (dirname (__file__), "EXAMPLE.SQLITE"))

	con = CREATE_SQLITE_DB (
		DB_LABEL = ORGAN_PATH
	)
	
	TISSUE = "CONCEPTS"
	TISSUE = "concepts"

	cur = con.cursor ()
	cur.execute (f"CREATE TABLE { TISSUE }(COMMAND, PID, STDOUT, STDERR)")
	
	#cur.execute(f"""
	#	INSERT INTO { TISSUE } VALUES
	#		(1, "1", "1"),
	#		(2, "2", "2")
	#""")
	
	cur.execute(f"INSERT INTO { TISSUE } VALUES ('python3', 2, '2', '2')")
	
	CYTOS = cur.execute(f"SELECT COMMAND, PID, STDOUT, STDERR FROM { TISSUE }").fetchall()
	print (CYTOS)
	
	res = cur.execute("SELECT name FROM sqlite_master")
	res.fetchone ()
	
	print (res)
	
	"""
		CONCEPTS {
			"PID": "",
			"STDOUT": [{
				
			}],
			"STDERR": [{
				
			}]
		}
	
	"""

	os.remove (ORGAN_PATH)

	return;
