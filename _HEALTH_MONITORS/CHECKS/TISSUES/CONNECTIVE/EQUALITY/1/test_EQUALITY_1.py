

"""
	./CHECKS start --path "TISSUES/CONNECTIVE/EQUALITY/1/test_EQUALITY_1.py"
"""

from WOMA_BIOLOGY.TISSUES.CONNECTIVE.EQUALITY import EQUALITY

def test_1 ():
	from os.path import normpath, join, dirname
	RESULTS = EQUALITY (
		normpath (join (dirname (__file__), "1")),
		normpath (join (dirname (__file__), "2")),
		
		RELATIVE_PATHS = True
	);
	
	print (RESULTS)