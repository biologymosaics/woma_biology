

"""
	./CHECKS start --path "TISSUES/CONNECTIVE/EQUALITY/2/test_EQUALITY_2.py"
"""

from WOMA_BIOLOGY.TISSUES.CONNECTIVE.EQUALITY import EQUALITY



def test_1 ():

	from os.path import normpath, join, dirname
	RESULTS = EQUALITY (
		normpath (join (dirname (__file__), "1")),
		normpath (join (dirname (__file__), "2")),
		
		RELATIVE_PATHS = True
	);
	
	import json
	assert (json.dumps (RESULTS), "{'1': {'3': 'd', '1.txt': 'fc'}, '2': {'1.txt': 'fc'}}")
	
	print (RESULTS)