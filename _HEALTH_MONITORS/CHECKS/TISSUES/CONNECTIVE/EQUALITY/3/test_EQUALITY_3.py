

"""
	./CHECKS start --path "TISSUES/CONNECTIVE/EQUALITY/3/test_EQUALITY_3.py"
"""

from WOMA_BIOLOGY.TISSUES.CONNECTIVE.EQUALITY import EQUALITY



def test_1 ():
	from os.path import normpath, join, dirname
	RESULTS = EQUALITY (
		normpath (join (dirname (__file__), "1")),
		normpath (join (dirname (__file__), "2")),
		
		RELATIVE_PATHS = True
	);

	PREDETERMINED = '{"1": {"3": "d", "1.txt": "fc"}, "2": {"9.txt": "f", "1.txt": "fc"}}'

	import json
	assert (json.dumps (RESULTS) == PREDETERMINED)
	
