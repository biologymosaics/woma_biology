
"""
	./CHECKS start --path "TISSUES/CONNECTIVE/GET_LIST/test_GET_LIST_1.py"
"""

from WOMA_BIOLOGY.TISSUES.CONNECTIVE.GET_LIST import GET_LIST


#----------------------------------------------------------------------------

import json

def test_MULTIPLE_1 ():
	from os.path import normpath, join, dirname
	PATH = normpath (join (dirname (__file__), "[READ_ONLY_CONNECTIVE]"))
	
	LIST = GET_LIST (PATH, REL_PATHS = True)
	
	
	print (json.dumps (LIST, indent = 2))
	
	assert ("""[
  {
    "PATH": "8",
    "KIND": "C"
  },
  {
    "PATH": "8/O.py",
    "KIND": "E"
  },
  {
    "PATH": "9.py",
    "KIND": "E"
  },
  {
    "PATH": "444",
    "KIND": "C"
  },
  {
    "PATH": "9",
    "KIND": "C"
  },
  {
    "PATH": "9/O.py",
    "KIND": "E"
  }
]""" == json.dumps (LIST, indent = 2))
	
	return;
