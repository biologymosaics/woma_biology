





"""
	from POLY_CONCEPTS_CHECK import CREATE_POLY
	
	POLY =  CREATE_POLY (
		CONCEPTS = [
			{ 
				"STRING": 'python3 -m http.server 9000',
				"CWD": None
			},
			{
				"STRING": 'python3 -m http.server 9001',
				"POPEN": {
					"cwd": "",
					"env": ""
				}
			}
		]
	)
	
	EXIT = POLY["EXIT"]
	

	EXIT ()
"""

"""
	POLY = CREATE_POLY (
		CONCEPTS = [
			'python3 -m http.server 9000', 
			'python3 -m http.server 9001'
		],
		
		#
		#	IF WAIT IS TRUE, STOPS CONTINUATION OF THE SCRIPT
		#
		#	IF WAIT IS FALSE, THE CONCEPTS EXIT WHEN PARENT CONCEPT EXITS
		#
		WAIT = True
	)
"""

import 	sys
from 	subprocess 		import Popen
import 	subprocess
import 	shlex
import 	atexit

def CREATE_POLY (
	CONCEPTS = [],
	WAIT = False
):
	PROCS = []

	for PROCESS in CONCEPTS:
		if (type (PROCESS) == str):
			print ("STARTING PROCESS", PROCESS)
		
			PROCS.append (
				Popen (
					shlex.split (PROCESS)
				)
			)
		
		elif (type (PROCESS) == dict):
			# KWARGS???
			print ("STARTING PROCESS", PROCESS)
		
			PROCESS_STRING = PROCESS["STRING"]
		
			CWD = None
			if ("CWD" in PROCESS):
				CWD = PROCESS["CWD"]
			
			ENV = None
			if ("ENV" in PROCESS):
				ENV = PROCESS["ENV"]
		
			#
			#	PRESET ARE "None" (not a string)
			#
			STDOUT = sys.stdout
			STDERR = sys.stderr
		
			CONCEPT = subprocess.Popen (
				shlex.split (PROCESS_STRING), 

				bufsize 			= - 1,
				close_fds  			= True, 
				creationflags		= 0, 
				cwd 				= CWD, 	
				encoding			= None,
				env					= ENV, 
				errors				= None, 
				executable 			= None, 
				extra_groups		= None, 
				group				= None, 
				
				restore_signals		= True, 
				
				shell				= False, 
				
				stdin 				= None, 
				stdout 				= STDOUT,
				stderr 				= STDERR,
				
				
				
				pass_fds			= (), 
				pipesize			= - 1, 
				preexec_fn 			= None, 
				#process_group		= None,
				
				start_new_session	= False, 
				startupinfo			= None, 
				
				text				= None, 
				
				umask				= - 1, 
				universal_newlines	= None, 
				user				= None, 
				
				#*, 
			)

	
	def EXIT ():
		print ("ATEXIT EXITING")

		for PROCESS in PROCS:
			print ("ENDING PROCESS ATEXIT", PROCESS);
			PROCESS.kill ()

		#exit ();

	atexit.register (EXIT)
	
			
	if (WAIT):
		for P in PROCS:
			#
			#	https://docs.python.org/3/library/subprocess.html#subprocess.Popen.wait
			#
			P.wait ()	
		
	return {
		"CONCEPTS": CONCEPTS,
		"EXIT": EXIT
	}
	
	
	
	