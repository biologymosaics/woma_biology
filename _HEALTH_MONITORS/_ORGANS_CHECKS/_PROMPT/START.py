


#PATH = "RIBOSOMES/STRING/test_LOCATE_CHAR.py"
#PATH = "RIBOSOMES/STRING/test_LOCATE_STRING.py"
#PATH = "RIBOSOMES/STRING/test_LOCATE_STRING.py::test_MULTIPLE_5"

#PATH = "SEAHORSE/SQLITE/test_SH_A.py"

#PATH = "CONCEPTS/POLY/test_POLY_B.py"

from POLY_CONCEPTS_CHECKS import CREATE_POLY

import click
@click.command ("start")
#@click.option ('-f', '--foreground', is_flag = True, default = False)
@click.option ('-p', '--path', default = '')
def START (path):
	PATH = path
	
	def ENV ():
		import os

		ENV = os.environ;

		for E in ENV:
			ENV.pop (E)

		ENV["PATH"] = ":".join ([
			"/bin",
			"/usr/bin"
		])

		ENV["PYTHONPATH"] = "./_ORGANS"
	#
	#------------------------------------------

	SHELL = "/bin/tcsh"

	#-----------------------------------------------

	from pathlib import Path
	HERE = Path (__file__).parent.absolute ()

	from os.path import normpath, join 
	PYTEST = normpath (join (HERE, "../../_ORGANS_CHECKS_PIP/bin/pytest"))

	def SEQUENCE ():
		return " ".join ([
			PYTEST,
			
			f'-sv { PATH }' if (len (PATH) >= 1) else '-v',
					
			'--ignore-glob="_ORGANS/**/*"'
		])

	print (SEQUENCE ())

	import subprocess
	PROC = subprocess.Popen (
		SEQUENCE (),

		env = ENV (),

		shell = True,
		executable = '/bin/tcsh'
	)

	PROC.wait ()










