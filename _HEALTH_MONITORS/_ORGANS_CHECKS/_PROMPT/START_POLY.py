


#PATH = "RIBOSOMES/STRING/test_LOCATE_CHAR.py"
#PATH = "RIBOSOMES/STRING/test_LOCATE_STRING.py"
#PATH = "RIBOSOMES/STRING/test_LOCATE_STRING.py::test_MULTIPLE_5"

#PATH = "SEAHORSE/SQLITE/test_SH_A.py"

#PATH = "CONCEPTS/POLY/test_POLY_B.py"

from POLY_CONCEPTS_CHECKS import CREATE_POLY

import click
@click.command ("start-poly")
@click.option ('-p', '--path', default = '')
def START (path):
	PATH = path
	
	def ENV ():
		import os

		ENV = os.environ;

		for E in ENV:
			ENV.pop (E)

		ENV["PATH"] = ":".join ([
			"/bin",
			"/usr/bin"
		])

		ENV["PYTHONPATH"] = "./_ORGANS"
	#
	#------------------------------------------

	SHELL = "/bin/tcsh"

	#--------------------------------------------
	
	def POLY_SEQUENCE (PATH):
		return {
			"STRING": " ".join ([
				'py.test-3',
				
				f'-sv { PATH }' if (len (PATH) >= 1) else '',
				
				'--ignore-glob="_ORGANS/**/*"'
			]),
			"ENV": ENV ()
		}
	
	from os.path import normpath, join, dirname
	GLOB_PATH = normpath (join (dirname (__file__), "../..")) + "/**/*__POLY.py"

	print (GLOB_PATH)

	import glob
	files = glob.glob (GLOB_PATH, recursive = True)
	print (files)

	CONCEPTS = list (map (lambda F : POLY_SEQUENCE (F), files))
	
	print ("CONCEPTS", CONCEPTS)


	POLY = CREATE_POLY (

		
		CONCEPTS = CONCEPTS,
		
		WAIT = True
	)
	


	print ("???")









