



"""
	from PYTHON_BIOLOGY.SIMULTANEOUS.BATCH import BATCH
	
	import asyncio
	asyncio.run(BATCH (
		FUNCTION = PROCEDURE__,
		PARAMS = [
			[ "2_1", 1 ],
			[ "2_2", 1 ],
			[ "2_3", 1 ]
		],
		MAX = 3
	))
"""

import asyncio

async def BATCH (
	FUNCTION = False,
	MAX = 10,
	PARAMS = []
):
	RESULTS = [];
	LOOP = asyncio.get_event_loop ()

	
	#
	# SEMAPHORES LIMIT THE NUMBER OF
	# ASYNC ACTIONS
	#
	SEM = asyncio.Semaphore (MAX)

	async def START (I, ACTIVITY):
		await SEM.acquire ()
		
		try:
			INDEX = I[0]
					
			await ACTIVITY (
				INDEX = I[0],
				PARAMS = PARAMS[INDEX]
			)
						
		finally:
			SEM.release()
			
		return;
	
	
	# TASK CREATION STARTS THE COROUTINES
	#
	#	https://docs.python.org/3/library/asyncio-future.html?highlight=ensure_future
	#
	TASKS = [
		asyncio.ensure_future (
			START (I, FUNCTION)
		)
		for I in enumerate (PARAMS)
	]
	

	#
	# WAIT FOR ALL OF THE 
	# FUNCTIONS TO COMPLETE
	#
	#	https://docs.python.org/3/library/asyncio-task.html?highlight=gather#asyncio.gather
	#
	await asyncio.gather (* TASKS)

	return