


#https://docs.python.org/3/library/asyncio-task.html#asyncio.create_task
	
async def main():
	task1 = asyncio.create_task (
		#say_after (1, 'hello')
		
		print ("??")
	)

	task2 = asyncio.create_task(
		#say_after (2, 'world')
		
		print ("????")
	)

	print (f"started at {time.strftime('%X')}")

	# Wait until both tasks are completed (should take
	# around 2 seconds.)
	await task1
	await task2

	print (f"finished at {time.strftime('%X')}")
	
#async def main():
#	await asyncio.sleep(1)
#	print ('hello')

from datetime import datetime
import asyncio
asyncio.run(main())