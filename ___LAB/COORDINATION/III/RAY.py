
'''
	( setenv RAY_DEDUP_LOGS 0 && python3 III/RAY.py )
'''


import ray

ray.init ()

# Define the functions.

@ray.remote
def ACTION (PARAMS):
	print ("ACTION", PARAMS)

	Z = 1
	while Z <= 10:
		print (PARAMS)

		import time
		time.sleep (1)
		
		Z += 1

	return PARAMS;

@ray.remote
def solve1(a):
	return 1

@ray.remote
def solve2(b):
	return 2

# Start two tasks in the background.
x_id = ACTION.remote (0)
y_id = ACTION.remote (1)

# Block until the tasks are done and get the results.
x, y = ray.get([x_id, y_id])
