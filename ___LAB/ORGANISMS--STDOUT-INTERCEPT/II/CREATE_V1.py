

'''
	https://docs.python.org/3/library/subprocess.html#subprocess.Popen.communicate
'''

from subprocess import Popen
import shlex
import subprocess
import sys

def CREATE (
	PROCESS_STRING,
	
	CWD = None,
	ENV = None
):
	'''
		If capture_output is true, stdout and stderr will be captured. 
		When used, the internal Popen object is automatically created 
		with stdout=PIPE and stderr=PIPE. The stdout and stderr arguments 
		may not be supplied at the same time as capture_output. 
		
		If you wish to capture and combine both streams into one, 
		use stdout=PIPE and stderr=STDOUT instead of capture_output.
	'''

	CONCEPT = Popen (
		shlex.split (PROCESS_STRING),
		
		cwd = CWD,
		env = ENV,
		
		stdout = subprocess.PIPE
	)
	

	return CONCEPT;
