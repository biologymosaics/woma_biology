

'''
	https://docs.python.org/3/library/subprocess.html#subprocess.Popen.communicate
'''

'''
	https://codereview.stackexchange.com/questions/6567/redirecting-subprocesses-output-stdout-and-stderr-to-the-logging-module
'''

'''
	https://docs.python.org/3/library/io.htmlst
'''

from subprocess import Popen
import shlex
import subprocess
import sys

from io import StringIO
import io


'''
	https://docs.python.org/3/library/io.html#io.TextIOBase

	import io

	output = io.StringIO()
	output.write('First line.\n')
	print('Second line.', file=output)

	# Retrieve file contents -- this will be
	# 'First line.\nSecond line.\n'
	contents = output.getvalue()

	# Close object and discard memory buffer --
	# .getvalue() will now raise an exception.
	output.close()


'''

def CREATE (
	PROCESS_STRING,
	
	CWD = None,
	ENV = None
):
	import sys

	'''
		If capture_output is true, stdout and stderr will be captured. 
		When used, the internal Popen object is automatically created 
		with stdout=PIPE and stderr=PIPE. The stdout and stderr arguments 
		may not be supplied at the same time as capture_output. 
		
		If you wish to capture and combine both streams into one, 
		use stdout=PIPE and stderr=STDOUT instead of capture_output.
	'''
	
	'''
		A stream—really, any object with a fileno method. 
		Popen will find the descriptor for that stream, using stream.fileno(), 
		and then proceed as for an int value.
		
		stream.fileno()
	'''
	

	import logging
	import sys
	log = logging.getLogger(__name__)
	out_hdlr = logging.StreamHandler(sys.stdout)
	out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
	out_hdlr.setLevel(logging.INFO)
	log.addHandler(out_hdlr)
	log.setLevel(logging.INFO)
	
	#FILE_WRITE_STREAM = open("output.txt", "w")
	#print(f.fileno())
	
	
	CONCEPT = Popen (
		shlex.split (PROCESS_STRING),
		
		cwd = CWD,
		env = ENV,
		
		#
		#	THE PRESET??
		#
		#stdout = sys.stdout
		
		#stdout = open("output.txt", "w")
		
		
		#stdout = subprocess.PIPE,
		#stdout = outfile,
		
		#stdout = stream
	)
	
	#out, err = CONCEPT.communicate ()
	

	return CONCEPT;
