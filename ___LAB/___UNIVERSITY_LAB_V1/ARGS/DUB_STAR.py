



def DUB_STAR (** PARAMETERS):
	for K, V in PARAMETERS.items ():
		print (K, V)

	return;
	
DUB_STAR (
	S1 = 1,
	S2 = 2,
	S3 = 3,
	S4 = 4,
	S5 = 5
);

print ("---------------------------------------")

def foo (name, /, ** KWDS):
	print ("name", name)
	
	for K, V in KWDS.items ():
		print (K, V)

	return 'name' in KWDS

foo (
	1, 
	**{
		'name': 2
	}
)