
"""
	SPECIAL PARAMETERS:
		https://docs.python.org/3/tutorial/controlflow.html#special-parameters

	*args
	
	
	**kwargs
"""
def STAR_1 (*				 PARAMETERS):
	for P in PARAMETERS:
		print (P)

	return;
	
	
STAR_1 (1,2,3,4,5)

#--------------------------------------------------------

def STAR_2 (I1, *					PARAMETERS):
	print ("PARAMETERS")

	for P in PARAMETERS:
		print (P)

	return;
	
STAR_2 (1,2,3,4,5)