





"""
# https://bash.cyberciti.biz/guide/Sending_signal_to_Processes

SIGINT
	kill -2

SIGKILL
	kill -9
	
SIGTERM
	kill -15 



ps -ax | grep rsync

	D
		uninterruptable sleep

	D+
		uninterruptable sleep
		in foreground process group

"""

import subprocess


def PERFORM (
	PERFORMANCE, 
	ALLOW = [ 0 ]
):
	try:
		result = subprocess.call (
			PERFORMANCE,
			shell = True
		)

		if (result not in ALLOW):
			print (f'result code "{ result }" not allowed');
			print ("\nexiting <- failure");
			exit ();

	except Exception as x:
		print ("Anomaly Occurred")
		print (x)
		exit ();

	print ("~~~~~~~~~~~~")
	print (PERFORMANCE, result)
	print ("~~~~~~~~~~~~")

	return;




PID = 7205

for x in range (1, 65):
	print (x)

	PERFORM (f"kill -{x} { PID }");



