



from io import StringIO
from subprocess import Popen, PIPE

with Popen (
	#[ 'python3', '_EXAMPLE.py' ], 
	[ 'df', '-h' ], 
	
	stdout  = PIPE, 
	bufsize = 1,
	universal_newlines = True
) as p, StringIO () as buf:

	for line in p.stdout:
		print (line, end='')
		buf.write (line)


	output = buf.getvalue()
	
	
	
rc = p.returncode