
import subprocess, sys
from os.path import normpath, join, dirname

file_stdout = open (normpath (join (dirname (__file__), 'RECORDS/STDOUT')), "w")
file_stderr = open (normpath (join (dirname (__file__), 'RECORDS/STDERR')), "w")

print (file_stdout)
print (file_stderr)

file_stdout = sys.stdout
file_stderr = sys.stderr


proc = subprocess.Popen (
	[ "python3", "_EXAMPLE.py" ],
	#[ "df", "-h" ],
	
	stdout = file_stdout, 
	stderr = file_stderr, 
	#shell = True
)

proc.wait ()