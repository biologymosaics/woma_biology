


import subprocess
from time import sleep

def stream_process(process):
	go = process.poll () is None
	
	for line in process.stdout:
		print (line)
		
	return go

PROC = [ "python3", "_EXAMPLE.py" ]
process = subprocess.Popen (
	PROC, 
	#shell=True, 
	stdout = subprocess.PIPE, 
	stderr = subprocess.STDOUT
)


while stream_process (process):
	sleep (0.1)