


import asyncio

async def ACTION (F):
	print ('STARTED', F)
	await asyncio.sleep (3 / (F + 1))
	print ('finished', F)
	
	return F

async def main ():
	values = await asyncio.gather (
		* [ ACTION (F) for F in range (10) ]
	)
	
	print (values)

asyncio.run (main ())