


import asyncio

async def ACTION (F):
	print ('STARTED', F)
	await asyncio.sleep (3 / (F + 1))
	print ('finished', F)
	
	return F

async def main ():
	async with asyncio.TaskGroup() as tg:
		task1 = tg.create_task (ACTION (10))
		task2 = tg.create_task (ACTION (5))
		
		
	print("Both tasks have completed now.")


asyncio.run (main ())
