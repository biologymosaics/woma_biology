


from subprocess import Popen, PIPE

with Popen(
	["python3", "EXAMPLE.py"], 
	stdout=PIPE, 
	bufsize=1,
	universal_newlines = True
) as p:
    
	for line in p.stdout:
		print(line, end='')