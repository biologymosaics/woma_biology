

from subprocess import Popen, PIPE, STDOUT
import pty
import os

cmd = 'python3 _EXAMPLE.py'

master, slave = pty.openpty()

p = Popen(cmd, shell=True, stdin=PIPE, stdout=slave, stderr=slave, close_fds=True)
stdout = os.fdopen (master)
print (stdout.readline())
print (stdout.readline())