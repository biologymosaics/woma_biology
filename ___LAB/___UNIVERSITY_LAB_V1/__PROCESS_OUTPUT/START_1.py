




"""
	Problem, the process doesn't stop...
	until the terminal is closed.............
"""

def CREATE_T_IO_WRAPPER ():
	import io

	reader = io.BufferedReader (io.BytesIO ("Lorem ipsum".encode("utf-8")))
	wrapper = io.TextIOWrapper (reader)
	#wrapper.read()  # returns Lorem ipsum

	return wrapper

print ("???")

import sys, subprocess
PROC = [ "python3", "_EXAMPLE.py" ]
#PROC = "python3 _EXAMPLE.py"

#
#	<_io.TextIOWrapper name='<stdout>' mode='w' encoding='utf-8'>
#
print (sys.stdout)

OUTPUT = CREATE_T_IO_WRAPPER ()
print (OUTPUT)


p = subprocess.Popen (
	PROC,
	
	#stdout = sys.stdout,
	stdout = OUTPUT,
	
	stderr = sys.stderr
)

p.wait ()