
cmd = [ "python3", "_EXAMPLE.py" ]
cmd = "python3 _EXAMPLE.py"

#cmd = "sleep 5; echo 1; sleep 5; echo 1"
#cmd = "python3 _EXAMPLE_2.py"

import subprocess

from time import sleep

def stream_process(process):
    go = process.poll() is None
    for line in process.stdout:
        print(line)
    return go

process = subprocess.Popen (cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
while stream_process(process):
    sleep(0.1)
