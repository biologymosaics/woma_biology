
import subprocess

cmd = "python3 _EXAMPLE.py"
cmd = "sleep 1; echo 1; sleep 1; echo 1"
cmd = "_EXAMPLE.py"

process = subprocess.Popen (
    cmd,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    #close_fds=True,
    #**kw
)

while True:
    reads, _, _ = select(
        [process.stdout.fileno(), process.stderr.fileno()],
        [], []
    )

    for descriptor in reads:
        if descriptor == process.stdout.fileno():
            read = process.stdout.readline()
            if read:
                print ('stdout: %s' % read)

        if descriptor == process.stderr.fileno():
            read = process.stderr.readline()
            if read:
                print ('stderr: %s' % read)
        sys.stdout.flush()

    if process.poll() is not None:
        break