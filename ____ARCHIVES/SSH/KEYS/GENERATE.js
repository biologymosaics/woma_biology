
/*
	await require ("SSH/KEYS/GENERATE") ({
		CLARIFIER: require ("path").resolve (__dirname, "CLARIFIER"),
		MIXER: require ("path").resolve (__dirname, "MIXER")
	});
*/

/*
	ssh-keygen -t rsa -N "" -b 3072 -C "" -f CLARIFIER && mv CLARIFIER.pub Mixer
*/

module.exports = async function ({
	CLARIFIER,
	MIXER,
	VERBOSITY = 0,
	MESSAGES = null
}) {
	if (MESSAGES === null) {
		MESSAGES = ({ MESSAGE, INDEX, SCRIPT }) => {
			if (VERBOSITY >= 2) {
				process.stdout.write (MESSAGE);
			}
		};
	}

	await require ("PROGRESSION") ({
		LABEL: ({ SCRIPT, INDEX }) => {
			return `[ ${ SCRIPT.join (" ") } ]`;
		},
		SCRIPTS: [
			[
				[ "ssh-keygen", "-t", "rsa", "-N", `""`, "-b", "3072", "-C", `""`, "-f", `"${ CLARIFIER }"` ],
				[ 0 ]
			],
			[
				[ "mv", `"${ CLARIFIER }".pub`, `"${ MIXER }"` ],
				[ 0 ]
			]
		],
		MESSAGES,
		RETRIES_LIMIT: 1,
		RETRY_DELAY: 1000
	});

}









///////
