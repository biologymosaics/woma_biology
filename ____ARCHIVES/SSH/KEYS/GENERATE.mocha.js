
var TEMPORARY = require ("path").resolve (
	__dirname,
	"[TEMPORARY]"
);

describe ("GENERATE", () => {
	it ("WORKS", async () => {
		var CLARIFIER = require ("path").resolve (TEMPORARY, "CLARIFIER");
		var MIXER = require ("path").resolve (TEMPORARY, "MIXER");

		await require ("./GENERATE") ({
			CLARIFIER,
			MIXER
		});

		await new Promise (F => {
			require ("fs").access (CLARIFIER, require ("fs").constants.F_OK, (err) => {
				if (err) throw err;

				F ();
			});
		});
		await new Promise (F => {
			require ("fs").rmdir (CLARIFIER, {
				recursive: true
			}, (err) => {
				F ();
			})
		});


		await new Promise (F => {
			require ("fs").access (MIXER, require ("fs").constants.F_OK, (err) => {
				if (err) throw err;

				F ();
			});
		});
		await new Promise (F => {
			require ("fs").rmdir (MIXER, {
				recursive: true
			}, (err) => {
				F ();
			})
		});
	});
});
