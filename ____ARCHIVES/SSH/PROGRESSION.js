
/*
	await require ("SSH/PROGRESSION") ({
		SCRIPTS: [
			[ "setenv SHELLLLSS asdf", [ 0 ] ],
			[ "echo $SHELLLLSS" ]
		],
		SSH: {
			ADDRESS: "172.17.0.2",
			PORT: 22,
			USER: "root",
			CLARIFIER_LOCATION: "/ENVIRONMENT/MODULES/LAUDE/__ACADEMY__/KEYS/CLARIFIER"
		},
		MESSAGES: ({ INDEX, SCRIPT, TYPE, MESSAGE }) => {

		}
	});
*/


/*


*/


var Client = require ('ssh2').Client;

module.exports = async function ({
	SCRIPTS,
	SSH,
	MESSAGES = ({ INDEX, SCRIPT, TYPE, MESSAGE }) => {
		console.log (INDEX, TYPE, MESSAGE);
	}
}) {
	var CONNECTION = new Client ();

	var CLARIFIER_STRAND = await new Promise (FULLFIL => {
		require ("fs").readFile (SSH.CLARIFIER_LOCATION, "utf8", (err, STRAND) => {
			if (err) throw err;

			FULLFIL (STRAND);
		});
	});

	await new Promise (FULLFIL => {
		CONNECTION.on ('ready', function() {
			(async () => {
				for (let A = 0; A < SCRIPTS.length; A++) {
					const SCRIPT = SCRIPTS [ A ] [ 0 ];
					const EXITS = SCRIPTS [ A ] [ 1 ] || [ 0 ];

					var OUT_STRAND_QUEUE = "";
					var ERROR_STRAND_QUEUE = "";

					await new Promise (F => {
						CONNECTION.exec (SCRIPT, function (err, stream) {
							if (err) throw err;

							stream.
							on ('close', function (CODE, SIGNAL) {
								MESSAGES ({
									INDEX: A,
									TYPE: "CLOSE",
									MESSAGE: `CLOSED WITH CODE ${ CODE } AND SIGNAL ${ SIGNAL }`,
									SCRIPT,
									CODE,
									SIGNAL
								});

								if (!EXITS.includes (CODE)) {
									throw new Error (`SCRIPT "${ SCRIPT }" exited with code ${ CODE } instead of one of [${ EXITS }]`)
								}

								F ();
							}).
							on ('data', function (STRAND) {
								MESSAGES ({
									INDEX: A,
									TYPE: "OUT",
									MESSAGE: STRAND.toString (),
									SCRIPT
								});
							}).
							stderr.
							on ('data', function (STRAND) {
								MESSAGES ({
									INDEX: A,
									TYPE: "ERROR",
									MESSAGE: STRAND.toString (),
									SCRIPT
								});
							});
						});
					});
				}

				CONNECTION.end ();

				FULLFIL ();
			}) ();
		}).
		connect ({
			host: SSH.ADDRESS,
			port: SSH.PORT,
			username: SSH.USER,
			privateKey: CLARIFIER_STRAND
		});
	});
}
