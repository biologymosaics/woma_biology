

/*
	HOPES:
		await require ("SSH/SEND") ({
			CLARIFIER: "",
			FROM: {
				LOCATION:
			},
			TO: {
				ADDRESS:
				LOCATION:
			}
		});

*/


/*
	scp -i {CLARIFIER} {REGION} root@{ADDRESS}:/root/A.txt


	scp -i {CLARIFIER} {REGION} root@{ADDRESS}:/root/A.txt

*/

// var PRESETS = {
// 	CLARIFIER:
// 	FROM: {
// 		LOCATION:
// 	},
// 	TO: {
// 		ADDRESS:
// 		LOCATION:
// 	}
// }


module.exports = async function (MACROS) {
	var { CLARIFIER, FROM, TO } = MACROS;


	var SCRIPT = [
		"scp",
		"-i",
		CLARIFIER,
		FROM.LOCATION,
		`root@${ TO.ADDRESS }:${ TO.LOCATION }`
	];

	await require ("PROGRESSION") ({
		LABEL: () => {
			return ``;
		},
		SCRIPTS: [
			[
				SCRIPT,
				[ 0 ]
			]
		],
		MESSAGES: ({ MESSAGE, INDEX, SCRIPT }) => {
			process.stdout.write (MESSAGE);
		},
		RETRIES_LIMIT: 1,
		RETRY_DELAY: 1000
	});
}
