

/*


*/

function isNumeric (n) {
    return !isNaN (parseFloat(n)) && isFinite(n);
}

module.exports = function(path) {
    var new_path = "";
    var chars = path.split ("");
    var non_specials = /[A-Za-z]/;

    for (var i=0; i < chars.length; i++) {
        if (non_specials.exec (chars[i]) === null) {
            new_path += '\\' + chars[i];
        }
        else {
            new_path += chars[i];
        }
    }

    return new_path;
}
