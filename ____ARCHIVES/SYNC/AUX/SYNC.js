

/*
	-o StrictHostKeyChecking=no


	rsync -r -a --info=progress2 -v --delete  --delete-excluded --progress --human-readable -e "ssh -o StrictHostKeyChecking=no -i '/ENVIRONMENT/CHOICES/MODULES/ISLANDS/__CHECKS__/[TEMPORARY]/OPEN_&_CLOSE/CLARIFIER'" '/ENVIRONMENT/CHOICES/MODULES/ISLANDS/__CHECKS__/[FIXTURES]/OPEN_&_CLOSE/A/' root@172.17.0.3:/root/A
*/

var log = console.log;

var ESCAPE = require ("./ESCAPE");

var BALLAD = require ("BALLAD");

module.exports = async function (OPTIONS) {
	var FROM = OPTIONS.FROM;
	var EXCLUDES = OPTIONS.FROM.EXCLUDES || [];

	var TO = OPTIONS.TO;
	var ADDRESS = TO.ADDRESS || false;
	var KEY = OPTIONS.KEY || null;

	var SCRIPT = [
		"rsync",
		"-r",
		"-a",
		"--info=progress2",
		"-v",

		// Delete Locations that have been erased
		"--delete",

		// EXCLUDES?????,
		EXCLUDES.map (EXCLUDE => {
			return `--exclude=${ ESCAPE (EXCLUDE )}`
		}).join (" "),

		"--delete-excluded",
		"--progress",
		"--human-readable",


		(() => {
			if (typeof ADDRESS === "string") {
				return [
					"-e",
					`"ssh`,
					`-o`,
					`StrictHostKeyChecking=no`,
					`-i`,
					`'${ KEY }'"`,
					`'${ FROM.DIRECTORY }/'`,
					`${ TO.NAME }@${ TO.ADDRESS }:${
						TO.DIRECTORY
					}`
				].join (" ")
			}
			else {
				return [
					FROM.DIRECTORY + '/',
					TO.DIRECTORY
				].join (" ")
			}
		}) ()
	].join (" ");

	if (typeof OPTIONS.TO.ADDRESS === "undefined") {
		await new Promise (FINISH => {
			require ("fs").
			mkdir (TO.DIRECTORY, { recursive: true }, (err) => {
				if (err) throw err;

				console.log ("MADE", TO.DIRECTORY);

				FINISH ();
			});
		});
	}

	console.log ({ SCRIPT })

	var { Code } = await BALLAD ({
		Label: OPTIONS.LABEL,
		Script: SCRIPT,
		Messages: ({ Message }) => {
			OPTIONS.MESSAGES ({ MESSAGE: Message });
		}
	});

	return;
}








//////
