

var SYNC = require ("./../../index.js");

// rsync -r -v A/ B

module.exports = function ({ test }) {
	test ('START', async CHECK => {
		await new Promise (FULFILL => {
			require ('fs').rmdir (require ("path").resolve (__dirname, "B"), {
				recursive: true
			}, (err) => {
				if (err) throw err;

				FULFILL ();
			});
		});

		await SYNC ({
			FROM: {
				DIRECTORY: require ("path").resolve (__dirname, "A")
			},
			TO: {
				DIRECTORY: require ("path").resolve (__dirname, "B")
			},
			MESSAGES: ({ MESSAGE }) => {
				process.stdout.write (MESSAGE);
			}
		});

		CHECK.pass ();
	});
}
