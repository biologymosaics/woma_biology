
// 'rsync -av --delete --exclude=A\\/B --delete-excluded --progress --human-readable -e "ssh -i CLARIFIER" \\.\\/A/ root@example.com:\\/A'

/*
	rsync -r -a --info=progress2 -v --delete --delete-excluded --progress --human-readable -e "ssh -i '/ENVIRONMENT/CAMP/KEYS/DANCES/SSH/Clarifier' './A' root@143.110.229.47:/B"

	rsync -r -a --info=progress2 -v --delete --delete-excluded --progress --human-readable -e "ssh -i /ENVIRONMENT/CAMP/KEYS/DANCES/SSH/Clarifier" A/ root@143.110.229.47:/B
*/

/*
	rsync -av --delete --delete-excluded --progress --human-readable -e "ssh -i /ENVIRONMENT/CAMP/KEYS/DANCES/SSH/Clarifier" A/ root@143.110.229.47:\/B
*/

/*
	WITH DOCKER FOR SSH????

*/

var SYNC = require ("./../../index.js");

SYNC ({
	FROM: {
		DIRECTORY: require ("path").resolve (__dirname, "A")
		// DIRECTORY: "A/"

	},
	TO: {
		DIRECTORY: "/B",
		KEY: "/ENVIRONMENT/CAMP/KEYS/DANCES/SSH/Clarifier",
		NAME: "root",
		ADDRESS: "143.110.229.47",
	},
	MESSAGES: ({ MESSAGE }) => {
		process.stdout.write (MESSAGE);
	}
});
