
/*
	await require ('SYNC') ({
		// OPTIONAL ///////////////
		WATCH: [],
		////////////////////////

		FROM: {
			DIRECTORY: <local directory>,
		},
		TO: {
			DIRECTORY: <remote directory>
		},

		MESSAGES: ({ MESSAGE }) => {
			process.stdout.write (MESSAGE);
		}
	});
*/

var SYNC = require ("./AUX/SYNC");
var CHOKIDAR = require ("chokidar");

var PRESETS = {
	MESSAGES: ({ MESSAGE }) => {
		console.log (MESSAGE);
	}
};

module.exports = function (MACROS) {
	var OPTIONS = require ("lodash/merge") ({}, PRESETS, MACROS);

	return new Promise (async (FULFILL) => {
		await SYNC (OPTIONS);
		FULFILL ();

		if (typeof OPTIONS.WATCH === "string") {
			const watcher = CHOKIDAR.watch (OPTIONS.WATCH, {
				persistent: true,
				ignoreInitial: true
			}).
			on ('all', (event, path) => {
				SYNC (OPTIONS);
			});

			return;
		}
	});
}
